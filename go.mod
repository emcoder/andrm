module gitlab.com/semure/andrm

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/gotk3/gotk3 v0.4.0
	github.com/pelletier/go-toml v1.8.0
)
