package main

import (
	"gitlab.com/semure/andrm/internal/server"
)

func main() {
	server.Server()
}
