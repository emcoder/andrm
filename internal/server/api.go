package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/licenses"
	"gitlab.com/semure/andrm/internal/logger"
)

func indexHandler(response http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(response, "Go Simple Licensing Server")
}

type Result struct {
	Error  string `json:"error,omitmepty"`
	Result string `json:"result,omitempty"`
}

func checkHandler(response http.ResponseWriter, request *http.Request) {
	request.ParseForm()
	license := request.FormValue("license")

	ip := strings.Split(request.RemoteAddr, ":")[0]
	checkResult, err := licenses.CheckLicense(license, ip)

	var result Result

	if err != nil {
		msg := "License check error: " + err.Error()
		logger.Error(msg)
		result = Result{
			msg,
			"",
		}
	} else {
		logger.Info("License check result:", checkResult)
		result = Result{
			"",
			checkResult,
		}
	}

	json, err := json.Marshal(result)
	if err != nil {
		logger.Error("Failed to parse JSON:", err)
		http.Error(response, err.Error(), http.StatusInternalServerError)
		return
	}

	response.Header().Set("Content-Type", "application/json")
	response.Write(json)
}

func api() {
	router := mux.NewRouter()
	router.HandleFunc("/", indexHandler)
	router.HandleFunc("/check", checkHandler).Methods("POST")
	http.Handle("/", router)
	if config.SSL {
		err := http.ListenAndServeTLS(":"+string(config.PORT), "server.crt", "server.key", nil) //:443
		if err != nil {
			logger.Error("SSL Server Error:", err)
			os.Exit(0)
		}
	} else {
		logger.Info("Listening on port", config.PORT)
		err := http.ListenAndServe(":"+strconv.Itoa(int(config.PORT)), nil)
		if err != nil {
			logger.Error("API server error:", err)
		}
	}
}
