package server

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/helpers"
)

func setup() {
	fmt.Println("Server Setup")
	fmt.Println("")
	var tmpconfig string = config.ConfigRaw

SetupPort:
	fmt.Print("Port # to Run Server On: ")
	scan := bufio.NewScanner(os.Stdin)
	scan.Scan()
	a, _ := helpers.ConvertInt(scan.Text())
	if !a {
		fmt.Println("Port can only be numbers")
		goto SetupPort
	}
	tmpconfig = strings.Replace(tmpconfig, "{PORT}", scan.Text(), -1)
SetupSSL:
	fmt.Print("Use SSL (true/false): ")
	scan = bufio.NewScanner(os.Stdin)
	scan.Scan()
	c, _ := helpers.ConvertBool(scan.Text())
	if !c {
		fmt.Println("Must be true or false")
		goto SetupSSL
	}
	tmpconfig = strings.Replace(tmpconfig, "{SSL}", scan.Text(), -1)

	fmt.Println("Generating Secure Key...")
	tmpconfig = strings.Replace(tmpconfig, "{KEY}", helpers.RandomString(16), -1)
	fmt.Print("SQL Database Host (127.0.0.1:3306): ")
	scan = bufio.NewScanner(os.Stdin)
	scan.Scan()
	tmpconfig = strings.Replace(tmpconfig, "{HOST}", scan.Text(), -1)

	fmt.Print("SQL Database: ")
	scan = bufio.NewScanner(os.Stdin)
	scan.Scan()
	tmpconfig = strings.Replace(tmpconfig, "{DB}", scan.Text(), -1)

	fmt.Print("SQL Database Username: ")
	scan = bufio.NewScanner(os.Stdin)
	scan.Scan()
	tmpconfig = strings.Replace(tmpconfig, "{USERNAME}", scan.Text(), -1)

	fmt.Print("SQL Database Password: ")
	scan = bufio.NewScanner(os.Stdin)
	scan.Scan()
	tmpconfig = strings.Replace(tmpconfig, "{PASSWORD}", scan.Text(), -1)

	fmt.Println("")
	fmt.Println("Saving config.toml file...")
	d1 := []byte(tmpconfig)
	err := ioutil.WriteFile("config.toml", d1, 0644)
	if err != nil {
		fmt.Println("There was an error creating the config file, please do it manually.")
	}
	if helpers.CheckFileExist("config.toml") {
		fmt.Println("Setup Finished.")
	}
}
