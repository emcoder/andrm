package server

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/helpers"
	ui "gitlab.com/semure/andrm/internal/ui"
)

func count() int { //Count Bot Rows
	rows, err := config.DB.Query("SELECT COUNT(*) AS count FROM licenses")
	if err != nil {
		return 0
	}
	var count int

	defer rows.Close()
	for rows.Next() {
		rows.Scan(&count)
	}
	return count
}

func Server() {
	fmt.Println("AnDRM Licensing System")

	if config.SSL {
		if !helpers.CheckFileExist("server.crt") || !helpers.CheckFileExist("server.key") {
			fmt.Println("[!] WARNING MAKE SURE YOU HAVE YOUR SSL FILES IN THE SAME DIR [!]")
			os.Exit(0)
		}
	}

	if !helpers.CheckFileExist("config.toml") {
		setup()
	}

	config.LoadConfig()

	db, err := sql.Open("mysql", config.USERNAME+":"+config.PASSWORD+"@tcp("+config.HOST+")/"+config.DATABASE+"?parseTime=true")
	if err != nil {
		fmt.Println("[!] ERROR: CHECK MYSQL SETTINGS! [!] {}", err)
		os.Exit(0)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil && !config.NO_CHECK {
		fmt.Println("[!] ERROR: CHECK IF MYSQL SERVER IS ONLINE! [!]")
		os.Exit(0)
	}

	config.DB = db

	go api()

	ui.UI()
}
