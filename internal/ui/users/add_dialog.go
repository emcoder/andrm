package usersUI

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/ui/style"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

type AddUserDialog struct {
	Dialog            *gtk.Dialog
	EmailEntry        *widgets.EntryWithLabel
	NameEntry         *widgets.EntryWithLabel
	GenderComboBox    *widgets.ComboBox
	BirthDateCalendar *gtk.Calendar
}

func AddUserDialogNew() AddUserDialog {
	appWindow := widgets.Widgets["appWindow"].(*gtk.ApplicationWindow)
	dialog, _ := gtk.DialogNewWithButtons("Add user", appWindow, gtk.DIALOG_MODAL, []interface{}{"Add", gtk.RESPONSE_ACCEPT}, []interface{}{"Cancel", gtk.RESPONSE_CANCEL})
	dialogBox, _ := dialog.GetContentArea()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	dialogBox.SetMarginStart(style.MARGIN_LARGE)
	dialogBox.SetMarginTop(style.MARGIN_LARGE)
	dialogBox.SetMarginEnd(style.MARGIN_LARGE)
	dialogBox.SetMarginBottom(style.MARGIN_LARGE)
	dialogBox.SetSpacing(style.MARGIN_LARGE)

	emailEntry := widgets.EntryWithLabelNew("Email:")
	nameEntry := widgets.EntryWithLabelNew("Name:")

	calendar, _ := gtk.CalendarNew()
	calendarLabel, _ := gtk.LabelNew("Date of birth:")
	calendarLabel.SetHAlign(gtk.ALIGN_START)
	calendarBox, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	calendarBox.Add(calendarLabel)
	calendarBox.Add(calendar)

	genderComboBox := getGenderComboBox()
	genderComboBox.SetID(0)

	box.Add(emailEntry.Container())
	box.Add(nameEntry.Container())
	box.Add(genderComboBox.Container())
	box.Add(calendarBox)

	dialogBox.Add(box)

	addDialog := AddUserDialog{
		dialog,
		&emailEntry,
		&nameEntry,
		&genderComboBox,
		calendar,
	}

	dialog.Connect("response", func(_ *gtk.Dialog, response gtk.ResponseType) {
		if response != gtk.RESPONSE_ACCEPT {
			dialog.Destroy()
			return
		}

		addDialog.handleAdd()
	})

	return addDialog
}

func (dialog *AddUserDialog) Run() {
	dialog.Dialog.ShowAll()
}
