package usersUI

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/logger"
	"gitlab.com/semure/andrm/internal/ui/widgets"
	"gitlab.com/semure/andrm/internal/users"
)

var Store UsersStore = UsersStoreNew()

type UsersStore struct {
	Users     []*users.User
	ListStore *gtk.ListStore
}

const (
	USERS_COLUMN_ID = iota
	USERS_COLUMN_EMAIL
	USERS_COLUMN_NAME
	USERS_COLUMN_BIRTH_DATE
	USERS_COLUMN_GENDER
)

func UsersStoreNew() UsersStore {
	listStore, _ := gtk.ListStoreNew(
		glib.TYPE_STRING, // ID
		glib.TYPE_STRING, // Email
		glib.TYPE_STRING, // Name
		glib.TYPE_STRING, // Birth date
		glib.TYPE_STRING, // Gender
	)
	return UsersStore{
		make([]*users.User, 0),
		listStore,
	}
}

func (*UsersStore) Refresh() {
	users, err := users.GetUsers(nil)

	if err != nil {
		widgets.ShowError("Failed to get users: "+err.Error(), nil)
		return
	}

	Store.Clear()

	for _, user := range users {
		Store.AddUser(user)
	}
}

func (store *UsersStore) Clear() {
	store.ListStore.Clear()
}

func (*UsersStore) AddUser(user *users.User) {
	id := int(user.ID)

	email := user.Email
	name := user.Name
	birthDate := user.BirthDate.Format("2006-01-02")
	gender := user.Gender.Name

	listStore := Store.ListStore
	// Get an iterator for a new row at the end of the list store
	iter := listStore.Append()

	// Set the contents of the list store row that the iterator represents
	err := listStore.Set(iter,
		[]int{
			USERS_COLUMN_ID,
			USERS_COLUMN_EMAIL,
			USERS_COLUMN_NAME,
			USERS_COLUMN_BIRTH_DATE,
			USERS_COLUMN_GENDER,
		},
		[]interface{}{
			id,
			email,
			name,
			birthDate,
			gender,
		},
	)

	if err != nil {
		logger.Error("Unable to add row:", err)
	}
}
