package usersUI

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/ui/widgets"
	"gitlab.com/semure/andrm/internal/users"
)

// Add a column to the tree view (during the initialization of the tree view)
func createUsersColumn(title string, id int) *gtk.TreeViewColumn {
	cellRenderer, _ := gtk.CellRendererTextNew()

	column, _ := gtk.TreeViewColumnNewWithAttribute(title, cellRenderer, "text", id)

	return column
}

type UsersPage struct {
	Box      *gtk.Box
	TreeView *gtk.TreeView
}

func (usersPage *UsersPage) getSelectedID() (uint, error) {
	return widgets.GetTreeViewSelectedID(
		usersPage.TreeView,
		Store.ListStore,
		USERS_COLUMN_ID,
	)
}

func BuildUsersPage() UsersPage {
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	toolbar, _ := gtk.ToolbarNew()

	createIcon, _ := gtk.ImageNewFromIconName("list-add", gtk.ICON_SIZE_SMALL_TOOLBAR)
	createButton, _ := gtk.ToolButtonNew(createIcon, "Add")

	editIcon, _ := gtk.ImageNewFromIconName("document-edit-symbolic", gtk.ICON_SIZE_SMALL_TOOLBAR)
	editButton, _ := gtk.ToolButtonNew(editIcon, "Edit")

	refreshIcon, _ := gtk.ImageNewFromIconName("view-refresh", gtk.ICON_SIZE_SMALL_TOOLBAR)
	refreshButton, _ := gtk.ToolButtonNew(refreshIcon, "Refresh")

	deleteIcon, _ := gtk.ImageNewFromIconName("delete", gtk.ICON_SIZE_SMALL_TOOLBAR)
	deleteButton, _ := gtk.ToolButtonNew(deleteIcon, "Delete")

	sep, _ := gtk.SeparatorToolItemNew()

	toolbar.Add(createButton)
	toolbar.Add(editButton)
	toolbar.Add(deleteButton)
	toolbar.Add(sep)
	toolbar.Add(refreshButton)

	box.Add(toolbar)

	treeView, _ := gtk.TreeViewNew()
	treeView.AppendColumn(createUsersColumn("ID", USERS_COLUMN_ID))
	treeView.AppendColumn(createUsersColumn("Email", USERS_COLUMN_EMAIL))
	nameColumn := createUsersColumn("Name", USERS_COLUMN_NAME)
	nameColumn.SetExpand(true)
	treeView.AppendColumn(nameColumn)
	treeView.AppendColumn(createUsersColumn("Date of birth", USERS_COLUMN_BIRTH_DATE))
	treeView.AppendColumn(createUsersColumn("Gender", USERS_COLUMN_GENDER))

	treeView.SetModel(Store.ListStore)
	treeView.SetHExpand(true)
	treeView.SetVExpand(true)

	box.Add(treeView)

	/* Actions */
	Store.Refresh()

	refreshButton.Connect("clicked", func() {
		Store.Refresh()
	})

	createButton.Connect("clicked", func() {
		addUserDialog := AddUserDialogNew()
		addUserDialog.Run()
	})

	page := UsersPage{
		box,
		treeView,
	}

	deleteButton.Connect("clicked", func() {
		page.handleDelete()
	})

	editButton.Connect("clicked", func() {
		userID, err := page.getSelectedID()

		if err != nil {
			widgets.ShowError(err.Error(), nil)
			return
		}

		user, err := users.GetUser(userID)

		if err != nil {
			widgets.ShowError(err.Error(), nil)
			return
		}

		editUserDialog := EditUserDialogNew(user)
		editUserDialog.Run()
	})

	return page
}

func (usersPage *UsersPage) Container() *gtk.Box {
	return usersPage.Box
}
