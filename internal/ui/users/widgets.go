package usersUI

import (
	"github.com/gotk3/gotk3/gtk"
	gendersUI "gitlab.com/semure/andrm/internal/ui/users/genders"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

func getGenderComboBox() widgets.ComboBox {
	store := gendersUI.Store
	store.Refresh()
	comboBox := widgets.ComboBoxNew("Gender:", store.ListStore)
	comboBox.ComboBox.SetIDColumn(gendersUI.GENDER_COLUMN_ID)

	cell, _ := gtk.CellRendererTextNew()
	comboBox.ComboBox.PackStart(cell, false)
	comboBox.ComboBox.AddAttribute(cell, "text", gendersUI.GENDER_COLUMN_NAME)

	return comboBox
}
