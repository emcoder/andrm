package usersUI

import (
	"fmt"

	"gitlab.com/semure/andrm/internal/ui/widgets"
	"gitlab.com/semure/andrm/internal/users"
	"gitlab.com/semure/andrm/internal/users/genders"
)

func (addDialog *AddUserDialog) handleAdd() {
	dialog := addDialog.Dialog
	email := addDialog.EmailEntry.GetText()
	name := addDialog.NameEntry.GetText()
	genderID, err := addDialog.GenderComboBox.GetID()

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	gender, err := genders.GetGender(genderID)

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	year, month, day := addDialog.BirthDateCalendar.GetDate()
	birthDate := fmt.Sprintf("%d-%02d-%02d", year, month+1, day)

	err = users.CreateUser(
		email,
		name,
		birthDate,
		gender,
	)

	if err != nil {
		widgets.ShowError(err.Error(), dialog)
		return
	}

	Store.Refresh()
	dialog.Destroy()
}
