package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/ui/style"
)

type SpinWithLabel struct {
	Box   *gtk.Box
	Label *gtk.Label
	Spin  *gtk.SpinButton
}

func SpinWithLabelNew(text string, from uint8, to uint8) SpinWithLabel {
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)
	label, _ := gtk.LabelNew(text)
	label.SetHAlign(gtk.ALIGN_START)
	spin, _ := gtk.SpinButtonNewWithRange(float64(from), float64(to), 1)

	box.Add(label)
	box.Add(spin)

	return SpinWithLabel{
		box,
		label,
		spin,
	}
}

func (entry *SpinWithLabel) Container() gtk.IWidget {
	return entry.Box
}

func (entry *SpinWithLabel) GetValue() uint8 {
	value := entry.Spin.GetValue()
	return uint8(value)
}
