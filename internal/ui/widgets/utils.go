package widgets

import (
	"errors"
	"strconv"

	"github.com/gotk3/gotk3/gtk"
)

// GetTreeViewSelectedID Gets selected grid object id
func GetTreeViewSelectedID(treeView *gtk.TreeView, store *gtk.ListStore, column int) (uint, error) {
	selection, _ := treeView.GetSelection()
	rows := selection.GetSelectedRows(store)

	if rows.Length() == 0 {
		return 0, errors.New("No row selected")
	}

	ids := make([]uint, 0, rows.Length())

	for l := rows; l != nil; l = l.Next() {
		path := l.Data().(*gtk.TreePath)
		iter, _ := store.GetIter(path)
		value, _ := store.GetValue(iter, column)
		str, _ := value.GetString()

		id, _ := strconv.Atoi(str)
		ids = append(ids, uint(id))
	}

	return ids[0], nil
}
