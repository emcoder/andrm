package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/ui/style"
)

type EntryWithLabel struct {
	Box   *gtk.Box
	Label *gtk.Label
	Entry *gtk.Entry
}

func EntryWithLabelNew(text string) EntryWithLabel {
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)
	label, _ := gtk.LabelNew(text)
	label.SetHAlign(gtk.ALIGN_START)
	entry, _ := gtk.EntryNew()

	box.Add(label)
	box.Add(entry)

	return EntryWithLabel{
		box,
		label,
		entry,
	}
}

func (entry *EntryWithLabel) Container() gtk.IWidget {
	return entry.Box
}

func (entry *EntryWithLabel) GetText() string {
	text, _ := entry.Entry.GetText()
	return text
}

func (entry *EntryWithLabel) SetText(text string) {
	entry.Entry.SetText(text)
}
