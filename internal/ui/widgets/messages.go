package widgets

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/logger"
)

func ShowError(msg string, parent gtk.IWindow) {
	logger.Error(msg)
	if parent == nil {
		parent = Widgets["appWindow"].(*gtk.ApplicationWindow)
	}

	errorDialog := gtk.MessageDialogNew(
		parent,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		gtk.MESSAGE_ERROR,
		gtk.BUTTONS_OK,
		"%s",
		msg,
	)

	errorDialog.Run()
	errorDialog.Destroy()
}

func ShowInfo(msg string, parent gtk.IWindow) {
	if parent == nil {
		parent = Widgets["appWindow"].(*gtk.ApplicationWindow)
	}

	dialog := gtk.MessageDialogNew(
		parent,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		gtk.MESSAGE_INFO,
		gtk.BUTTONS_OK,
		"%s",
		msg,
	)
	dialog.Run()
	dialog.Destroy()
}

func ShowConfirm(msg string, parent gtk.IWindow) gtk.ResponseType {
	if parent == nil {
		parent = Widgets["appWindow"].(*gtk.ApplicationWindow)
	}

	dialog := gtk.MessageDialogNew(
		parent,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		gtk.MESSAGE_WARNING,
		gtk.BUTTONS_YES_NO,
		"%s",
		msg,
	)
	response := dialog.Run()
	dialog.Destroy()

	return response
}
