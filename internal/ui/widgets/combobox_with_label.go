package widgets

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/gotk3/gotk3/gtk"
)

type ComboBox struct {
	Box      *gtk.Box
	Label    *gtk.Label
	ComboBox *gtk.ComboBox
	Model    gtk.ITreeModel
}

func ComboBoxNew(text string, model gtk.ITreeModel) ComboBox {
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 6)
	label, _ := gtk.LabelNew(text)
	label.SetHAlign(gtk.ALIGN_START)

	combobox, _ := gtk.ComboBoxNewWithModel(model)

	box.Add(label)
	box.Add(combobox)

	return ComboBox{
		box,
		label,
		combobox,
		model,
	}
}

func (combobox *ComboBox) Container() gtk.IWidget {
	return combobox.Box
}

func (comboBox *ComboBox) GetID() (uint, error) {
	idStr := comboBox.ComboBox.GetActiveID()

	if idStr == "" {
		return 0, errors.New("No row with ID selected in combobox")
	}

	id, err := strconv.Atoi(idStr)

	if err != nil {
		msg := fmt.Sprintf("Failed to parse ID \"%s\", error: %s", idStr, err)
		return 0, errors.New(msg)
	}

	return uint(id), nil
}

func (comboBox *ComboBox) SetID(id uint) {
	comboBox.ComboBox.SetActiveID(strconv.Itoa(int(id)))
}
