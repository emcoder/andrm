package productsUI

import (
	"github.com/gotk3/gotk3/gtk"
	typesUI "gitlab.com/semure/andrm/internal/ui/products/types"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

func getTypeComboBox() widgets.ComboBox {
	store := typesUI.Store
	store.Refresh()
	comboBox := widgets.ComboBoxNew("Type:", store.ListStore)
	comboBox.ComboBox.SetIDColumn(typesUI.PRODUCT_TYPE_COLUMN_ID)

	cell, _ := gtk.CellRendererTextNew()
	comboBox.ComboBox.PackStart(cell, false)
	comboBox.ComboBox.AddAttribute(cell, "text", typesUI.PRODUCT_TYPE_COLUMN_NAME)

	return comboBox
}
