package productsUIStore

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/logger"
	"gitlab.com/semure/andrm/internal/products"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

var Store ProductsStore = ProductsStoreNew()

type ProductsStore struct {
	Products  []*products.Product
	ListStore *gtk.ListStore
}

const (
	PRODUCTS_COLUMN_ID = iota
	PRODUCTS_COLUMN_NAME
	PRODUCTS_COLUMN_TYPE
	PRODUCTS_COLUMN_MIN_AGE
)

func ProductsStoreNew() ProductsStore {
	listStore, _ := gtk.ListStoreNew(
		glib.TYPE_STRING, // ID
		glib.TYPE_STRING, // Name
		glib.TYPE_STRING, // Type
		glib.TYPE_STRING, // MinAge
	)
	return ProductsStore{
		make([]*products.Product, 0),
		listStore,
	}
}

func (*ProductsStore) Refresh() {
	logger.Debug("Refresh products list...")
	products, err := products.GetProducts(nil)

	if err != nil {
		widgets.ShowError("Failed to get products: "+err.Error(), nil)
		return
	}

	Store.Clear()

	for _, product := range products {
		Store.AddProduct(product)
	}
}

func (store *ProductsStore) Clear() {
	store.ListStore.Clear()
}

func (*ProductsStore) AddProduct(product *products.Product) {
	listStore := Store.ListStore

	// Get an iterator for a new row at the end of the list store
	iter := listStore.Append()

	// Set the contents of the list store row that the iterator represents
	err := listStore.Set(iter,
		[]int{
			PRODUCTS_COLUMN_ID,
			PRODUCTS_COLUMN_NAME,
			PRODUCTS_COLUMN_TYPE,
			PRODUCTS_COLUMN_MIN_AGE,
		},
		[]interface{}{
			product.ID,
			product.Name,
			product.Type.Name,
			product.MinAge,
		},
	)

	if err != nil {
		logger.Error("Unable to add product row:", err)
	}
}
