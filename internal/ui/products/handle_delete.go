package productsUI

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/logger"
	"gitlab.com/semure/andrm/internal/products"
	licensesUIStore "gitlab.com/semure/andrm/internal/ui/licenses/store"
	store "gitlab.com/semure/andrm/internal/ui/products/store"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

func (page *ProductsPage) handleDelete() {
	logger.Debug("Delete product...")
	productID, err := page.getSelectedID()

	if err != nil {
		widgets.ShowError(err.Error(), nil)
		return
	}

	confirm := widgets.ShowConfirm("Are you sure you want to delete product?", nil)

	if confirm != gtk.RESPONSE_YES {
		return
	}

	err = products.DeleteProduct(productID)

	if err != nil {
		widgets.ShowError(err.Error(), nil)
	}

	store.Store.Refresh()
	licensesUIStore.Store.Refresh()
}
