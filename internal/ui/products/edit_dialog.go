package productsUI

import (
	"fmt"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/products"
	"gitlab.com/semure/andrm/internal/ui/style"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

type EditProductDialog struct {
	Product      *products.Product
	Dialog       *gtk.Dialog
	TypeComboBox *widgets.ComboBox
	NameEntry    *widgets.EntryWithLabel
	MinAgeSpin   *widgets.SpinWithLabel
}

func EditProductDialogNew(product *products.Product) EditProductDialog {
	appWindow := widgets.Widgets["appWindow"].(*gtk.ApplicationWindow)
	dialog, _ := gtk.DialogNewWithButtons(
		fmt.Sprintf("Edit product %s", product.Name),
		appWindow,
		gtk.DIALOG_MODAL,
		[]interface{}{"Save", gtk.RESPONSE_ACCEPT}, []interface{}{"Cancel", gtk.RESPONSE_CANCEL},
	)
	dialogBox, _ := dialog.GetContentArea()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	dialogBox.SetMarginStart(style.MARGIN_LARGE)
	dialogBox.SetMarginTop(style.MARGIN_LARGE)
	dialogBox.SetMarginEnd(style.MARGIN_LARGE)
	dialogBox.SetMarginBottom(style.MARGIN_LARGE)
	dialogBox.SetSpacing(style.MARGIN_LARGE)

	nameEntry := widgets.EntryWithLabelNew("Name:")
	nameEntry.SetText(product.Name)

	minAgeSpin := widgets.SpinWithLabelNew("Age:", 12, 120)
	minAgeSpin.Spin.SetValue(float64(product.MinAge))

	typeComboBox := getTypeComboBox()
	typeComboBox.SetID(product.Type.ID)

	box.Add(typeComboBox.Container())
	box.Add(nameEntry.Container())
	box.Add(minAgeSpin.Container())

	dialogBox.Add(box)

	editDialog := EditProductDialog{
		product,
		dialog,
		&typeComboBox,
		&nameEntry,
		&minAgeSpin,
	}

	dialog.Connect("response", func(_ *gtk.Dialog, response gtk.ResponseType) {
		if response != gtk.RESPONSE_ACCEPT {
			dialog.Destroy()
			return
		}

		editDialog.handleSave()
	})

	return editDialog
}

func (dialog *EditProductDialog) Run() {
	dialog.Dialog.ShowAll()
}
