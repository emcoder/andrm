package productTypesUI

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/logger"
	types "gitlab.com/semure/andrm/internal/products/types"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

var Store ProductTypesStore = ProductTypesStoreNew()

type ProductTypesStore struct {
	ProductTypes []*types.ProductType
	ListStore    *gtk.ListStore
}

const (
	PRODUCT_TYPE_COLUMN_ID = iota
	PRODUCT_TYPE_COLUMN_NAME
)

func ProductTypesStoreNew() ProductTypesStore {
	listStore, _ := gtk.ListStoreNew(
		glib.TYPE_STRING, // ID
		glib.TYPE_STRING, // Name
	)
	return ProductTypesStore{
		make([]*types.ProductType, 0),
		listStore,
	}
}

func (*ProductTypesStore) Refresh() {
	productTypes, err := types.GetProductTypes()

	if err != nil {
		widgets.ShowError("Failed to get product types: "+err.Error(), nil)
		return
	}

	Store.Clear()

	for _, productType := range productTypes {
		Store.Add(productType)
	}
}

func (store *ProductTypesStore) Clear() {
	store.ListStore.Clear()
}

func (*ProductTypesStore) Add(productType *types.ProductType) {
	id := int(productType.ID)
	name := productType.Name

	listStore := Store.ListStore
	// Get an iterator for a new row at the end of the list store
	iter := listStore.Append()

	// Set the contents of the list store row that the iterator represents
	err := listStore.Set(iter,
		[]int{
			PRODUCT_TYPE_COLUMN_ID,
			PRODUCT_TYPE_COLUMN_NAME,
		},
		[]interface{}{
			id,
			name,
		},
	)

	if err != nil {
		logger.Error("Unable to add row:", err)
	}
}
