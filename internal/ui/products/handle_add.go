package productsUI

import (
	"fmt"

	producttypes "gitlab.com/semure/andrm/internal/products/types"
	// "github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/products"
	store "gitlab.com/semure/andrm/internal/ui/products/store"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

func (dialog *AddProductDialog) handleAdd() {
	typeComboBox := dialog.TypeComboBox
	nameEntry := dialog.NameEntry
	ageSpin := dialog.MinAgeSpin

	typeID, err := typeComboBox.GetID()
	if err != nil {
		widgets.ShowError(fmt.Sprintf("Type is required: %s", err.Error()), dialog.Dialog)
		return
	}

	name := nameEntry.GetText()
	minAge := ageSpin.GetValue()

	productType, err := producttypes.GetProductType(typeID)
	if err != nil {
		widgets.ShowError(err.Error(), dialog.Dialog)
		return
	}

	err = products.CreateProduct(
		productType,
		name,
		minAge,
	)

	if err != nil {
		widgets.ShowError(err.Error(), dialog.Dialog)
		return
	}

	store.Store.Refresh()
	dialog.Dialog.Destroy()
}
