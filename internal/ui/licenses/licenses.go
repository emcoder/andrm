package licensesUi

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/licenses"
	store "gitlab.com/semure/andrm/internal/ui/licenses/store"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

// Add a column to the tree view (during the initialization of the tree view)
func createLicenseColumn(title string, id int) *gtk.TreeViewColumn {
	cellRenderer, _ := gtk.CellRendererTextNew()

	column, _ := gtk.TreeViewColumnNewWithAttribute(title, cellRenderer, "text", id)

	return column
}

type LicensesPage struct {
	Box      *gtk.Box
	TreeView *gtk.TreeView
}

func (page *LicensesPage) getSelectedID() (uint, error) {
	return widgets.GetTreeViewSelectedID(
		page.TreeView,
		store.Store.ListStore,
		store.LICENSES_COLUMN_ID,
	)
}

func BuildLicensesPage() gtk.IWidget {
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)

	createIcon, _ := gtk.ImageNewFromIconName("list-add", gtk.ICON_SIZE_SMALL_TOOLBAR)
	createButton, _ := gtk.ToolButtonNew(createIcon, "Add")

	editIcon, _ := gtk.ImageNewFromIconName("document-edit-symbolic", gtk.ICON_SIZE_SMALL_TOOLBAR)
	editButton, _ := gtk.ToolButtonNew(editIcon, "Edit")

	deleteIcon, _ := gtk.ImageNewFromIconName("delete", gtk.ICON_SIZE_SMALL_TOOLBAR)
	deleteButton, _ := gtk.ToolButtonNew(deleteIcon, "Delete")

	refreshIcon, _ := gtk.ImageNewFromIconName("view-refresh", gtk.ICON_SIZE_SMALL_TOOLBAR)
	refreshButton, _ := gtk.ToolButtonNew(refreshIcon, "Refresh")

	toolbar, _ := gtk.ToolbarNew()
	toolbar.Add(createButton)
	toolbar.Add(editButton)
	toolbar.Add(deleteButton)

	sep, _ := gtk.SeparatorToolItemNew()

	toolbar.Add(sep)
	toolbar.Add(refreshButton)

	box.Add(toolbar)

	treeView, _ := gtk.TreeViewNew()
	treeView.AppendColumn(createLicenseColumn("ID", store.LICENSES_COLUMN_ID))
	treeView.AppendColumn(createLicenseColumn("User", store.LICENSES_COLUMN_USER))
	treeView.AppendColumn(createLicenseColumn("Product", store.LICENSES_COLUMN_PRODUCT))
	treeView.AppendColumn(createLicenseColumn("License", store.LICENSES_COLUMN_KEY))
	treeView.AppendColumn(createLicenseColumn("Expiration", store.LICENSES_COLUMN_EXPIRATION))

	treeView.SetModel(store.Store.ListStore)
	treeView.SetHExpand(true)
	treeView.SetVExpand(true)

	box.Add(treeView)

	/* Actions */
	store.Store.Refresh()

	page := LicensesPage{
		box,
		treeView,
	}

	refreshButton.Connect("clicked", func() {
		store.Store.Refresh()
	})

	createButton.Connect("clicked", func() {
		addLicenseDialog := AddLicenseDialogNew()
		addLicenseDialog.Run()
	})

	deleteButton.Connect("clicked", func() {
		page.handleDelete()
	})

	editButton.Connect("clicked", func() {
		licenseID, err := page.getSelectedID()

		if err != nil {
			widgets.ShowError(err.Error(), nil)
			return
		}

		license, err := licenses.GetLicense(licenseID)

		if err != nil {
			widgets.ShowError(err.Error(), nil)
			return
		}

		editDialog := EditLicenseDialogNew(license)
		editDialog.Run()
	})

	return box
}
