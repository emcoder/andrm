package licensesUi

import (
	"fmt"
	"strconv"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/licenses"
	"gitlab.com/semure/andrm/internal/products"
	store "gitlab.com/semure/andrm/internal/ui/licenses/store"
	"gitlab.com/semure/andrm/internal/ui/style"
	"gitlab.com/semure/andrm/internal/ui/widgets"
	"gitlab.com/semure/andrm/internal/users"
)

func showKeyInfo(msg string, parent gtk.IWindow, key string) {
	dialog := gtk.MessageDialogNew(
		parent,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		gtk.MESSAGE_INFO,
		gtk.BUTTONS_OK,
		"%s",
		msg,
	)
	area, _ := dialog.GetContentArea()
	area.SetSpacing(style.MARGIN_LARGE)

	keyEntry, _ := gtk.EntryNew()
	keyEntry.SetText(key)
	keyEntry.SetEditable(false)
	keyEntry.SetMarginStart(style.MARGIN_LARGE)
	keyEntry.SetMarginEnd(style.MARGIN_LARGE)

	area.Add(keyEntry)
	keyEntry.Show()
	area.ShowAll()

	dialog.Run()
	dialog.Destroy()
}

func (dialog *AddLicenseDialog) handleAdd() {
	userIDStr := dialog.UserComboBox.ComboBox.GetActiveID()

	if userIDStr == "" {
		widgets.ShowError("User is required!", dialog.Dialog)
		return
	}

	userID, err := strconv.Atoi(userIDStr)

	if err != nil {
		msg := fmt.Sprintf("Failed to parse user ID \"%s\", error: %s", userIDStr, err)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	user, err := users.GetUser(uint(userID))

	if err != nil {
		msg := fmt.Sprintf("Failed to get user ID with ID = %d, error: %s", userID, err)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	productIDStr := dialog.ProductComboBox.ComboBox.GetActiveID()

	if productIDStr == "" {
		widgets.ShowError("Product is required!", dialog.Dialog)
		return
	}

	productID, err := strconv.Atoi(productIDStr)

	if err != nil {
		msg := fmt.Sprintf("Failed to parse user ID \"%s\", error: %s", userIDStr, err)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	product, err := products.GetProduct(uint(productID))

	year, month, day := dialog.Calendar.GetDate()
	expiration := fmt.Sprintf("%d-%02d-%02d", year, month+1, day)

	key, err := licenses.CreateLicense(user, product, expiration)

	if err != nil {
		msg := fmt.Sprintf(
			"Failed to add product %s license "+
				"for user %s (expiration: %s).\n"+
				"Error:\n%s",
			product.Name,
			user.Email,
			expiration,
			err,
		)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	msg := "License Key Generated!\n\n" +
		fmt.Sprintf("License Email: %s\n", user.Email) +
		fmt.Sprintf("License Experation: %s\n", expiration) +
		fmt.Sprintf("Save this as license.dat:")

	showKeyInfo(msg, dialog.Dialog, key)

	store.Store.Refresh()
	dialog.Dialog.Destroy()
}
