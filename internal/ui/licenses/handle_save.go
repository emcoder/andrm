package licensesUi

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/semure/andrm/internal/licenses"
	"gitlab.com/semure/andrm/internal/products"
	store "gitlab.com/semure/andrm/internal/ui/licenses/store"
	"gitlab.com/semure/andrm/internal/ui/widgets"
	"gitlab.com/semure/andrm/internal/users"
)

func (dialog *EditLicenseDialog) handleSave() {
	license := dialog.License

	userIDStr := dialog.UserComboBox.ComboBox.GetActiveID()

	if userIDStr == "" {
		widgets.ShowError("User is required!", dialog.Dialog)
		return
	}

	userID, err := strconv.Atoi(userIDStr)

	if err != nil {
		msg := fmt.Sprintf("Failed to parse user ID \"%s\", error: %s", userIDStr, err)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	user, err := users.GetUser(uint(userID))

	if err != nil {
		msg := fmt.Sprintf("Failed to get user ID with ID = %d, error: %s", userID, err)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	productIDStr := dialog.ProductComboBox.ComboBox.GetActiveID()

	if productIDStr == "" {
		widgets.ShowError("Product is required!", dialog.Dialog)
		return
	}

	productID, err := strconv.Atoi(productIDStr)

	if err != nil {
		msg := fmt.Sprintf("Failed to parse user ID \"%s\", error: %s", userIDStr, err)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	product, err := products.GetProduct(uint(productID))

	year, month, day := dialog.Calendar.GetDate()
	expiration := fmt.Sprintf("%d-%02d-%02d", year, month+1, day)

	license.User = user
	license.Product = product
	license.Expiration = time.Date(int(year), time.Month(int(month)+1), int(day), 0, 0, 0, 0, time.UTC)

	err = licenses.UpdateLicense(license)

	if err != nil {
		msg := fmt.Sprintf(
			"Failed to update product %s license "+
				"for user %s (expiration: %s).\n"+
				"Error:\n%s",
			product.Name,
			user.Email,
			expiration,
			err,
		)
		widgets.ShowError(msg, dialog.Dialog)
		return
	}

	store.Store.Refresh()
	dialog.Dialog.Destroy()
}
