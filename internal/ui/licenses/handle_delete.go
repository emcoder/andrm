package licensesUi

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/licenses"
	"gitlab.com/semure/andrm/internal/logger"
	store "gitlab.com/semure/andrm/internal/ui/licenses/store"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

func (page *LicensesPage) handleDelete() {
	logger.Debug("Delete license...")
	licenseID, err := page.getSelectedID()

	if err != nil {
		widgets.ShowError(err.Error(), nil)
		return
	}

	confirm := widgets.ShowConfirm("Are you sure you want to delete license?", nil)

	if confirm != gtk.RESPONSE_YES {
		return
	}

	err = licenses.DeleteLicense(licenseID)

	if err != nil {
		widgets.ShowError(err.Error(), nil)
	}

	store.Store.Refresh()
}
