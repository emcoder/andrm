package licensesUi

import (
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/ui/style"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

type AddLicenseDialog struct {
	Dialog          *gtk.Dialog
	UserComboBox    *widgets.ComboBox
	ProductComboBox *widgets.ComboBox
	Calendar        *gtk.Calendar
}

func AddLicenseDialogNew() AddLicenseDialog {
	appWindow := widgets.Widgets["appWindow"].(*gtk.ApplicationWindow)
	dialog, _ := gtk.DialogNewWithButtons(
		"Add license",
		appWindow,
		gtk.DIALOG_DESTROY_WITH_PARENT,
		[]interface{}{"Add", gtk.RESPONSE_ACCEPT},
		[]interface{}{"Cancel", gtk.RESPONSE_CANCEL},
	)
	dialogBox, _ := dialog.GetContentArea()
	box, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	dialogBox.SetMarginStart(style.MARGIN_LARGE)
	dialogBox.SetMarginTop(style.MARGIN_LARGE)
	dialogBox.SetMarginEnd(style.MARGIN_LARGE)
	dialogBox.SetMarginBottom(style.MARGIN_LARGE)
	dialogBox.SetSpacing(style.MARGIN_LARGE)

	userComboBox := getUserComboBox()

	productComboBox := getProductComboBox()

	calendar, _ := gtk.CalendarNew()
	calendarLabel, _ := gtk.LabelNew("Expiration:")
	calendarLabel.SetHAlign(gtk.ALIGN_START)
	calendarBox, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, style.MARGIN_SMALL)

	calendarBox.Add(calendarLabel)
	calendarBox.Add(calendar)

	box.Add(userComboBox.Container())
	box.Add(productComboBox.Container())
	box.Add(calendarBox)

	dialogBox.Add(box)

	licenseAddDialog := AddLicenseDialog{
		dialog,
		&userComboBox,
		&productComboBox,
		calendar,
	}

	dialog.Connect("response", func(_ *gtk.Dialog, response gtk.ResponseType) {
		if response != gtk.RESPONSE_ACCEPT {
			dialog.Destroy()
			return
		}

		licenseAddDialog.handleAdd()
	})

	return licenseAddDialog
}

func (dialog *AddLicenseDialog) Run() {
	dialog.Dialog.ShowAll()
}
