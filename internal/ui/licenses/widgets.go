package licensesUi

import (
	"github.com/gotk3/gotk3/gtk"
	productsUIStore "gitlab.com/semure/andrm/internal/ui/products/store"
	usersUI "gitlab.com/semure/andrm/internal/ui/users"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

func getUserComboBox() widgets.ComboBox {
	usersStore := usersUI.Store
	usersStore.Refresh()
	userComboBox := widgets.ComboBoxNew("User:", usersStore.ListStore)
	userComboBox.ComboBox.SetIDColumn(usersUI.USERS_COLUMN_ID)

	cell, _ := gtk.CellRendererTextNew()
	userComboBox.ComboBox.PackStart(cell, false)
	userComboBox.ComboBox.AddAttribute(cell, "text", usersUI.USERS_COLUMN_EMAIL)

	return userComboBox
}

func getProductComboBox() widgets.ComboBox {
	productsStore := productsUIStore.Store
	productsStore.Refresh()
	productComboBox := widgets.ComboBoxNew("Product:", productsStore.ListStore)
	productComboBox.ComboBox.SetIDColumn(productsUIStore.PRODUCTS_COLUMN_ID)

	cell, _ := gtk.CellRendererTextNew()
	productComboBox.ComboBox.PackStart(cell, false)
	productComboBox.ComboBox.AddAttribute(cell, "text", productsUIStore.PRODUCTS_COLUMN_NAME)

	return productComboBox
}
