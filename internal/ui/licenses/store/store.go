package licensesUIStore

import (
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/semure/andrm/internal/licenses"
	"gitlab.com/semure/andrm/internal/logger"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

var Store LicensesStore = LicensesStoreNew()

type LicensesStore struct {
	Licenses  []*licenses.License
	ListStore *gtk.ListStore
}

const (
	LICENSES_COLUMN_ID = iota
	LICENSES_COLUMN_USER
	LICENSES_COLUMN_PRODUCT
	LICENSES_COLUMN_KEY
	LICENSES_COLUMN_EXPIRATION
)

func LicensesStoreNew() LicensesStore {
	listStore, _ := gtk.ListStoreNew(
		glib.TYPE_STRING, // ID
		glib.TYPE_STRING, // User
		glib.TYPE_STRING, // Product
		glib.TYPE_STRING, // Key
		glib.TYPE_STRING, // Expiration
	)
	return LicensesStore{
		make([]*licenses.License, 0),
		listStore,
	}
}

func (*LicensesStore) Refresh() {
	licenses, err := licenses.GetLicenses()

	if err != nil {
		widgets.ShowError("Failed to get licenses: "+err.Error(), nil)
		return
	}

	Store.Clear()

	for _, license := range licenses {
		Store.AddLicense(license)
	}
}

func (store *LicensesStore) Clear() {
	store.ListStore.Clear()
}

func (*LicensesStore) AddLicense(license *licenses.License) {
	id := int(license.ID)

	user := license.User.String()

	product := license.Product.Name
	key := license.License
	expiration := license.Expiration.Format("2006-01-02")

	listStore := Store.ListStore

	// Get an iterator for a new row at the end of the list store
	iter := listStore.Append()

	// Set the contents of the list store row that the iterator represents
	err := listStore.Set(iter,
		[]int{
			LICENSES_COLUMN_ID,
			LICENSES_COLUMN_USER,
			LICENSES_COLUMN_PRODUCT,
			LICENSES_COLUMN_KEY,
			LICENSES_COLUMN_EXPIRATION,
		},
		[]interface{}{
			id,
			user,
			product,
			key,
			expiration,
		},
	)

	if err != nil {
		logger.Error("Unable to add row:", err)
	}
}
