package ui

import (
	"log"
	"os"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	licensesUI "gitlab.com/semure/andrm/internal/ui/licenses"
	productsUI "gitlab.com/semure/andrm/internal/ui/products"
	usersUI "gitlab.com/semure/andrm/internal/ui/users"
	"gitlab.com/semure/andrm/internal/ui/widgets"
)

// UI runs graphical interface for server
func UI() {
	// Create Gtk Application, change appID to your application domain name reversed.
	const appID = "org.gtk.andrm"
	application, err := gtk.ApplicationNew(appID, glib.APPLICATION_FLAGS_NONE)
	// Check to make sure no errors when creating Gtk Application
	if err != nil {
		log.Fatal("Could not create application.", err)
	}
	// Application signals available
	// startup -> sets up the application when it first starts
	// activate -> shows the default first window of the application (like a new document). This corresponds to the application being launched by the desktop environment.
	// open -> opens files and shows them in a new window. This corresponds to someone trying to open a document (or documents) using the application from the file browser, or similar.
	// shutdown ->  performs shutdown tasks
	// Setup Gtk Application callback signals
	application.Connect("activate", func() { onActivate(application) })
	// Run Gtk application
	os.Exit(application.Run(os.Args))
}

// Callback signal from Gtk Application
func onActivate(application *gtk.Application) {
	// Create ApplicationWindow
	appWindow, err := gtk.ApplicationWindowNew(application)
	widgets.Widgets["appWindow"] = appWindow
	if err != nil {
		log.Fatal("Could not create application window.", err)
	}
	// Set ApplicationWindow Properties
	appWindow.SetTitle("AnDRM")
	appWindow.SetDefaultSize(600, 400)

	ui := buildUI(appWindow)

	appWindow.Add(ui)
	appWindow.ShowAll()
}

func addExpirationDateSelector(grid *gtk.Grid) *gtk.Calendar {
	label, _ := gtk.LabelNew("Expiration date:")
	label.SetHAlign(gtk.ALIGN_END)
	label.SetVAlign(gtk.ALIGN_START)
	grid.Attach(label, 0, 1, 1, 1)

	calendar, _ := gtk.CalendarNew()
	grid.Attach(calendar, 1, 1, 1, 1)

	return calendar
}

func buildUI(window *gtk.ApplicationWindow) gtk.IWidget {
	notebook, _ := gtk.NotebookNew()

	usersPage := usersUI.BuildUsersPage()
	usersLabel, _ := gtk.LabelNew("Users")

	licensesPage := licensesUI.BuildLicensesPage()
	licensesLabel, _ := gtk.LabelNew("Licenses")

	productsPage := productsUI.BuildProductsPage()
	productsLabel, _ := gtk.LabelNew("Products")

	notebook.AppendPage(usersPage.Container(), usersLabel)
	notebook.AppendPage(licensesPage, licensesLabel)
	notebook.AppendPage(productsPage, productsLabel)

	// grid, _ := gtk.GridNew()
	// grid.SetColumnSpacing(10)
	// grid.SetRowSpacing(10)
	// grid.SetHExpand(true)
	// grid.SetVExpand(true)
	// grid.SetMarginEnd(10)
	// grid.SetMarginBottom(10)
	// grid.SetMarginStart(10)
	// grid.SetMarginTop(10)

	// label, _ := gtk.LabelNew("Email:")
	// label.SetHAlign(gtk.ALIGN_END)
	// grid.Attach(label, 0, 0, 1, 1)

	// emailEntry, _ := gtk.EntryNew()
	// emailEntry.SetHExpand(true)
	// grid.Attach(emailEntry, 1, 0, 1, 1)

	// calendar := addExpirationDateSelector(grid)

	// vbox, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	// vbox.SetVExpand(true)
	// grid.Attach(vbox, 0, 2, 2, 1)

	// button, _ := gtk.ButtonNewWithLabel("Generate license")
	// grid.Attach(button, 0, 3, 2, 1)

	// button.Connect("clicked", func() {
	// 	email, _ := emailEntry.GetText()
	// 	if email == "" {
	// 		showError(window, "Email is required")
	// 		return
	// 	}

	// 	year, month, day := calendar.GetDate()
	// 	expiration := fmt.Sprintf("%d-%02d-%02d", year, month, day)

	// 	key, err := licenses.AddLicense(email, expiration)

	// 	if err != nil {
	// 		msg := fmt.Sprintf("Failed to add license with email %s (expiration: %s).\nError:\n%s", email, expiration, err)
	// 		showError(window, msg)
	// 		return
	// 	}

	// 	msg := "License Key Generated!\n\n" +
	// 		fmt.Sprintf("License Email: %s\n", email) +
	// 		fmt.Sprintf("License Experation: %s\n", expiration) +
	// 		fmt.Sprintf("Save this as license.dat\n\n") +
	// 		key

	// 	showInfo(window, msg)
	// })

	return notebook
}
