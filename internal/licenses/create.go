package licenses

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/helpers"
	"gitlab.com/semure/andrm/internal/logger"
	"gitlab.com/semure/andrm/internal/products"
	"gitlab.com/semure/andrm/internal/users"
)

func genLicenseKey() string {
	return strings.Join([]string{
		helpers.RandomString(4),
		helpers.RandomString(4),
		helpers.RandomString(4),
	}, "-")
}

func CreateLicense(
	user *users.User,
	product *products.Product,
	expiration string,
) (string, error) {
	userAge := user.Age()
	if userAge < product.MinAge {
		msg := fmt.Sprintf(
			"Product age restriction violation: min age is %d, user age is %d",
			product.MinAge, userAge,
		)
		return "", errors.New(msg)
	}

	license := genLicenseKey()
	logger.Info("Generated license key:", license)
	db := config.DB

	var id uint
	err := db.QueryRow("SELECT id FROM licenses WHERE license='" + license + "'").Scan(&id)

	if err != nil && err != sql.ErrNoRows {
		msg := "Failed to check license existance: " + err.Error()
		logger.Error(msg)
		return "", errors.New(msg)
	}

	userID := user.ID
	productID := product.ID

	_, err = db.Exec(
		`INSERT INTO licenses(user_id, product_id, license, expiration, ip) VALUES(?, ?, ?, ?, ?)`,
		userID, productID, license, expiration, "none",
	)

	if err != nil {
		msg := "Failed to insert license into database: " + err.Error()
		logger.Error(msg)
		return "", errors.New(msg)
	}

	licenseKey, err := Encrypt(config.KEY, license)

	if err != nil {
		msg := "Failed to encrypt license: " + err.Error()
		logger.Error(msg)
		return "", errors.New(msg)
	}

	logger.Info("Encrypted license:", licenseKey)

	fmt.Println("License Key Generated!")
	fmt.Println("")
	fmt.Println("License User ID:", userID)
	fmt.Println("License Expiration:", expiration)
	fmt.Println("Save this as license.dat")
	fmt.Println("")
	fmt.Println(licenseKey)
	fmt.Println("")

	return licenseKey, nil
}
