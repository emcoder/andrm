package licenses

import (
	"errors"
	"fmt"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
)

// DeleteLicense deletes license by ID
func DeleteLicense(licenseID uint) error {
	db := config.DB

	_, err := db.Exec("DELETE FROM licenses WHERE id=?", licenseID)

	if err != nil {
		msg := fmt.Sprintf("Failed to delete license (id = %d): %s", licenseID, err.Error())
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
