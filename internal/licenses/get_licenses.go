package licenses

import (
	"errors"
	"log"
	"time"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/products"
	"gitlab.com/semure/andrm/internal/users"
)

type License struct {
	ID         uint
	User       *users.User
	Product    *products.Product
	License    string
	Expiration time.Time
}

// GetLicenses gets licenses
func GetLicenses() ([]*License, error) {
	log.Print("Get licenses...")

	db := config.DB
	rows, err := db.Query("SELECT id, user_id, product_id, license, expiration FROM licenses")
	if err != nil {
		msg := "Failed to get licenses:" + err.Error()
		log.Print(msg)
		return nil, errors.New(msg)
	}
	defer rows.Close()

	licenses := []*License{}

	for rows.Next() {
		var (
			id         uint
			userID     uint
			productID  uint
			license    string
			expiration time.Time
		)
		if err := rows.Scan(&id, &userID, &productID, &license, &expiration); err != nil {
			msg := "Failed to get licenses:" + err.Error()
			log.Print(msg)
			return nil, errors.New(msg)
		}

		user, _ := users.GetUser(userID)
		product, _ := products.GetProduct(productID)

		licenses = append(licenses, &License{
			id,
			user,
			product,
			license,
			expiration,
		})
		log.Printf("Got license: id=%d for user user_id=%d\n", id, userID)
	}

	return licenses, nil
}
