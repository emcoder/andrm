package licenses

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
)

func CheckLicense(license string, ip string) (string, error) {
	db := config.DB
	var tmpexp string
	decrypted, err := Decrypt(config.KEY, license)

	if err != nil {
		msg := "Failed to decrypt license: " + err.Error()
		logger.Error(msg)
		return "", errors.New(msg)
	}

	err = db.QueryRow("SELECT expiration FROM licenses WHERE license='" + decrypted + "'").Scan(&tmpexp)

	if err == sql.ErrNoRows { //No License for Key found
		return "Bad", nil
	}

	if err != nil {
		msg := "Failed to check license: " + err.Error()
		logger.Error(msg)
		return "", errors.New(msg)
	}

	//Check Expiration date
	_, err = db.Exec("UPDATE licenses SET ip='" + ip + "' WHERE license='" + decrypted + "'")
	if err != nil {
		logger.Error(err)
	}

	tmpexp = tmpexp[:10]
	t, err := time.Parse("2006-01-02", tmpexp)
	if err != nil {
		msg := fmt.Sprintf("Failed to parse date %s: %s", tmpexp, err.Error())
		logger.Error(msg)
		return "", errors.New(msg)
	}

	t2, _ := time.Parse("2006-01-02", time.Now().Format("2006-01-02"))
	if err != nil {
		msg := fmt.Sprintf("Failed to parse date: %s", err.Error())
		logger.Error(msg)
		return "", errors.New(msg)
	}

	if t.After(t2) {
		return "Good", nil
	} else {
		return "Expired", nil
	}
}
