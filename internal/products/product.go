package products

import types "gitlab.com/semure/andrm/internal/products/types"

type Product struct {
	ID     uint
	Type   *types.ProductType
	Name   string
	MinAge uint8
}
