package producttypes

import (
	"errors"
	"fmt"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
)

type ProductType struct {
	ID   uint
	Name string
}

func (productType *ProductType) String() string {
	return productType.Name
}

// GetProductTypes gets product types
func GetProductTypes() ([]*ProductType, error) {
	db := config.DB

	logger.Debug("Get product types...")

	query := "SELECT id, name FROM product_types ORDER BY name"
	logger.Trace("SQL Query:", query)

	rows, err := db.Query(query)
	if err != nil {
		msg := "Failed to get product types:" + err.Error()
		logger.Error(msg)
		return nil, errors.New(msg)
	}
	defer rows.Close()

	productTypes := []*ProductType{}

	for rows.Next() {
		var (
			id   uint
			name string
		)

		if err := rows.Scan(&id, &name); err != nil {
			msg := "Failed to get product types:" + err.Error()
			logger.Error(msg)
			return nil, errors.New(msg)
		}

		productTypes = append(productTypes, &ProductType{
			id,
			name,
		})
		logger.Debug("Got product type: id=%d, name=%s\n", id, name)
	}

	return productTypes, nil
}

// GetProductType returns one product type
func GetProductType(id uint) (*ProductType, error) {
	db := config.DB

	logger.Debug(fmt.Sprintf("Get product type (id=%d)...", id))

	query := "SELECT name FROM product_types WHERE id=?"
	logger.Trace("SQL Query:", query)

	row := db.QueryRow(query, id)

	var (
		name string
	)

	if err := row.Scan(&name); err != nil {
		msg := "Failed to get product type:" + err.Error()
		logger.Error(msg)
		return nil, errors.New(msg)
	}

	productType := &ProductType{
		id,
		name,
	}
	logger.Debug("Got product type: id=%d, name=%s\n", id, name)

	return productType, nil
}
