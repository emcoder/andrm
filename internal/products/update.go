package products

import (
	"errors"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
)

// UpdateProduct updates product data in DB
func UpdateProduct(product *Product) error {
	db := config.DB

	if product.Name == "" {
		return errors.New("Name is required")
	}

	if product.MinAge <= 0 {
		return errors.New("Min age is required")
	}

	_, err := db.Exec(
		"UPDATE products SET type_id=?, name=?, min_age=? WHERE id=?",
		product.Type.ID, product.Name, product.MinAge, product.ID,
	)

	if err != nil {
		msg := "Failed to update product:" + err.Error()
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
