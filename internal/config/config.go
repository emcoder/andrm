package config

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/pelletier/go-toml"
)

type Config struct {
	port int64
	ssl  bool
	key  string

	host     string
	database string
	username string
	password string
	no_check bool
}

var (
	PORT int64
	SSL  bool
	KEY  string

	HOST     string
	DATABASE string
	USERNAME string
	PASSWORD string
	NO_CHECK bool

	DB *sql.DB

	ConfigRaw string = `[server]
port = {PORT}
ssl = {SSL}
key = "{KEY}"

[database]
host = "{HOST}"
db = "{DB}"
username = "{USERNAME}"
password = "{PASSWORD}"
nocheck = false`
)

func LoadConfig() {
	config, err := toml.LoadFile("config.toml")
	if err != nil {
		fmt.Println("[ERROR] Could not load config.toml!")
		os.Exit(0)
	} else {
		PORT = config.Get("server.port").(int64)
		SSL = config.Get("server.ssl").(bool)
		KEY = config.Get("server.key").(string)

		HOST = config.Get("database.host").(string)
		DATABASE = config.Get("database.db").(string)
		USERNAME = config.Get("database.username").(string)
		PASSWORD = config.Get("database.password").(string)
		NO_CHECK = config.Get("database.nocheck").(bool)
	}
}
