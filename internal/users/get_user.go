package users

import (
	"errors"
	"fmt"
	"log"
)

// GetUser finds one user by id
func GetUser(id uint) (*User, error) {
	users, err := GetUsers(&[]uint{id})

	if err != nil {
		msg := "Failed to get users:" + err.Error()
		log.Print(msg)
		return nil, errors.New(msg)
	}

	if len(users) == 0 {
		msg := fmt.Sprintf("User id=%d not found", id)
		log.Print(msg)
		return nil, errors.New(msg)
	}

	user := users[0]

	return user, nil
}
