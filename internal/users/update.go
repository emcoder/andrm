package users

import (
	"database/sql"
	"errors"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
)

// UpdateUser updates user data in DB
func UpdateUser(user *User) error {
	db := config.DB

	if user.Email == "" {
		return errors.New("Email is required")
	}

	if user.Name == "" {
		return errors.New("Name is required")
	}

	if user.BirthDate.IsZero() {
		return errors.New("Birth date is required")
	}

	var genderID sql.NullInt32

	if user.Gender.ID == 0 {
		genderID = sql.NullInt32{
			0,
			false,
		}
	} else {
		genderID = sql.NullInt32{
			int32(user.Gender.ID),
			true,
		}
	}

	_, err := db.Exec(
		"UPDATE users SET email=?, name=?, birth_date=?, gender_id=? WHERE id=?",
		user.Email, user.Name, user.BirthDate.Format("2006-01-02"), genderID, user.ID,
	)

	if err != nil {
		msg := "Failed to update user:" + err.Error()
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
