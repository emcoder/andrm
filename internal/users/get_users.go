package users

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"gitlab.com/semure/andrm/internal/logger"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/users/genders"
)

type User struct {
	ID        uint
	Email     string
	Name      string
	BirthDate time.Time
	Gender    *genders.Gender
	AddressID uint
}

func (user *User) String() string {
	if user.Name != "" {
		return fmt.Sprintf("%s (%s)", user.Name, user.Email)
	}

	return user.Email
}

// GetUsers gets users
func GetUsers(ids *[]uint) ([]*User, error) {
	log.Print("Get users...")

	query := "SELECT id, email, name, birth_date, gender_id, address_id FROM users"
	args := []interface{}{}

	if ids != nil && len(*ids) > 0 {
		idsCount := len(*ids)

		idsArg := "("
		for index, id := range *ids {
			idsArg += fmt.Sprint(id)
			if index < idsCount-1 {
				idsArg += ","
			}
		}
		idsArg += ")"
		query += " WHERE id IN " + idsArg
	}

	log.Println("SQL Query:", query)
	db := config.DB
	rows, err := db.Query(query, args...)
	if err != nil {
		msg := "Failed to get users:" + err.Error()
		log.Print(msg)
		return nil, errors.New(msg)
	}
	defer rows.Close()

	users := []*User{}

	for rows.Next() {
		var (
			id        uint
			email     string
			name      string
			birthDate time.Time
			genderID  sql.NullInt32
			addressID sql.NullInt32
		)
		if err := rows.Scan(&id, &email, &name, &birthDate, &genderID, &addressID); err != nil {
			msg := "Failed to get users:" + err.Error()
			log.Print(msg)
			return nil, errors.New(msg)
		}

		var userGenderID uint = 0
		var userAddressID uint = 0

		if genderID.Valid {
			userGenderID = uint(genderID.Int32)
		}

		if addressID.Valid {
			userAddressID = uint(addressID.Int32)
		}

		gender, err := genders.GetGender(userGenderID)

		if err != nil {
			logger.Error("Failed to get user gender:", err)
			return nil, err
		}

		users = append(users, &User{
			id,
			email,
			name,
			birthDate,
			gender,
			userAddressID,
		})
		log.Printf("Got user: id=%d, email=%s\n", id, email)
	}

	return users, nil
}

func (user *User) Age() uint8 {
	return uint8(time.Since(user.BirthDate).Hours() / 24 / 365)
}
