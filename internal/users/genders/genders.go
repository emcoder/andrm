package genders

import (
	"errors"
	"fmt"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
)

type Gender struct {
	ID   uint
	Name string
}

func (gender *Gender) String() string {
	return gender.Name
}

// GetGenders gets genders
func GetGenders() ([]*Gender, error) {
	db := config.DB

	logger.Debug("Get genders...")

	query := "SELECT id, name FROM genders"
	logger.Trace("SQL Query:", query)

	rows, err := db.Query(query)
	if err != nil {
		msg := "Failed to get genders:" + err.Error()
		logger.Error(msg)
		return nil, errors.New(msg)
	}
	defer rows.Close()

	genders := []*Gender{}
	genders = append(genders, &Gender{
		0,
		"Unknown",
	})

	for rows.Next() {
		var (
			id   uint
			name string
		)

		if err := rows.Scan(&id, &name); err != nil {
			msg := "Failed to get genders:" + err.Error()
			logger.Error(msg)
			return nil, errors.New(msg)
		}

		genders = append(genders, &Gender{
			id,
			name,
		})
		logger.Debug("Got gender: id=%d, name=%s\n", id, name)
	}

	return genders, nil
}

// GetGender returns one gender
func GetGender(id uint) (*Gender, error) {
	if id == 0 {
		return &Gender{
			0,
			"Unknown",
		}, nil
	}

	db := config.DB

	logger.Debug(fmt.Sprintf("Get gender (id=%d)...", id))

	query := "SELECT name FROM genders WHERE id=?"
	logger.Trace("SQL Query:", query)

	row := db.QueryRow(query, id)

	var (
		name string
	)

	if err := row.Scan(&name); err != nil {
		msg := "Failed to get gender:" + err.Error()
		logger.Error(msg)
		return nil, errors.New(msg)
	}

	gender := &Gender{
		id,
		name,
	}
	logger.Debug("Got gender: id=%d, name=%s\n", id, name)

	return gender, nil
}
