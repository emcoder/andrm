package users

import (
	"database/sql"
	"errors"

	"gitlab.com/semure/andrm/internal/config"
	"gitlab.com/semure/andrm/internal/logger"
	"gitlab.com/semure/andrm/internal/users/genders"
)

// CreateUser creates new user
func CreateUser(
	email string,
	name string,
	birthDate string,
	gender *genders.Gender,
) error {
	db := config.DB

	if email == "" {
		return errors.New("Email is required")
	}

	if name == "" {
		return errors.New("Name is required")
	}

	if birthDate == "" {
		return errors.New("Birth date is required")
	}

	var genderID sql.NullInt32

	if gender.ID != 0 {
		genderID = sql.NullInt32{
			Int32: int32(gender.ID),
			Valid: true,
		}
	} else {
		genderID = sql.NullInt32{
			Int32: 0,
			Valid: false,
		}
	}

	_, err := db.Exec(
		"INSERT INTO users (email, name, birth_date, gender_id) VALUES (?, ?, ?, ?)",
		email, name, birthDate, genderID,
	)

	if err != nil {
		msg := "Failed to create user:" + err.Error()
		logger.Error(msg)
		return errors.New(msg)
	}

	return nil
}
