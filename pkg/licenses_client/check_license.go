package licensesclient

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"

	"gitlab.com/semure/andrm/internal/helpers"
	"gitlab.com/semure/andrm/internal/logger"
)

func errorHandle(err error) {
	fmt.Println("[ERROR]An error has occurred. Please contact your seller.")
	os.Exit(0)
}

func getUrlString(host string, port int, ssl bool) string {
	var urlStr string

	if ssl {
		urlStr = "https://"
	} else {
		urlStr = "http://"
	}

	urlStr += host

	if port > 0 {
		urlStr += ":" + strconv.Itoa(port)
	}

	urlStr += "/check"

	return urlStr
}

type Result struct {
	Result string `json:"result,omitempty"`
	Error  string `json:"error,omitempty"`
}

func CheckLicense(host string, port int, ssl bool, license string) (string, error) {
	urlStr := getUrlString(host, port, ssl)
	var client *http.Client

	if ssl {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = &http.Client{Transport: tr}
	} else {
		client = &http.Client{}
	}

	data := url.Values{}
	data.Set("license", string(license))

	req, err := http.NewRequest("POST", urlStr, bytes.NewBufferString(data.Encode()))
	if err != nil {
		logger.Error("Failed to create request:", err)
		return "", err
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	res, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)
	var result Result = Result{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		msg := fmt.Sprintf("Failed to parse JSON \"%s\": %s", string(body), err.Error())
		logger.Error(msg)
		return "", errors.New(msg)
	}

	if result.Error != "" {
		return "", errors.New(result.Error)
	}

	if res.StatusCode != 200 {
		msg := fmt.Sprintf("Unexpected API return HTTP status %d, result: %s", res.StatusCode, result)
		return "", errors.New(msg)
	}

	return result.Result, nil
}

// CheckLicenseForce checks license, prints result and fails if license is expired
func CheckLicenseForce(api string, ssl bool, silent bool) {
	if !helpers.CheckFileExist("license.dat") {
		if !silent {
			fmt.Println("license.dat not found.")
		}
		os.Exit(0)
	}

	li, err := ioutil.ReadFile("license.dat") //string(li)
	if err != nil {
		if !silent {
			errorHandle(err)
		}
	}

	var client *http.Client

	if ssl {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = &http.Client{Transport: tr}
	} else {
		client = &http.Client{}
	}

	data := url.Values{}
	data.Set("license", string(li))
	u, _ := url.ParseRequestURI(api + "check")
	urlStr := fmt.Sprintf("%v", u)
	r, _ := http.NewRequest("POST", urlStr, bytes.NewBufferString(data.Encode()))
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(r)
	if err != nil {
		if !silent {
			fmt.Println("Unable to connect to license server.")
		}
		os.Exit(0)
	}
	defer resp.Body.Close()
	respBody, _ := ioutil.ReadAll(resp.Body)
	result := string(respBody)
	logger.Info("License server response:", result)
	if resp.StatusCode == 200 {
		if string(result) != "Good" {
			if string(result) == "Expired" {
				if !silent {
					fmt.Println("License is Expired.")
				}
				os.Exit(0)
			} else {
				if !silent {
					fmt.Println("Connot verify license, Please contact your seller.")
				}
				os.Exit(0)
			}
		}
	}
}
